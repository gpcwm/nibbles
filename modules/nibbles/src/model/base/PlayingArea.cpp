/**
 * nibbles: PlayingArea.cpp
 */

#include "model/base/PlayingArea.h"

#include <sstream>
#include <stdexcept>

#include <boost/lexical_cast.hpp>

namespace nibbles {

//#################### CONSTRUCTORS ####################

PlayingArea::PlayingArea(int height, int width)
: m_cells(width * height),
  m_height(height),
  m_width(width)
{}

//#################### PUBLIC MEMBER FUNCTIONS ####################

int& PlayingArea::cell(int i, int j)
{
  ensure_bounds(i, j);
  return m_cells[i * m_width + j];
}

int PlayingArea::cell(int i, int j) const
{
  ensure_bounds(i, j);
  return m_cells[i * m_width + j];
}

int PlayingArea::height() const
{
  return m_height;
}

int PlayingArea::width() const
{
  return m_width;
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

void PlayingArea::ensure_bounds(int i, int j) const
{
  if(i < 0 || i >= m_height || j < 0 || j >= m_width)
  {
    throw std::runtime_error("Cell (" + boost::lexical_cast<std::string>(i) + "," + boost::lexical_cast<std::string>(j) + ") is out of bounds");
  }
}

}
