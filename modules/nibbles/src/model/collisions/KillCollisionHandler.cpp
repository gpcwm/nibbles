/**
 * nibbles: KillCollisionHandler.cpp
 */

#include "model/collisions/KillCollisionHandler.h"

namespace nibbles {

//#################### PRIVATE OPERATORS ####################

boost::optional<Object::Move> KillCollisionHandler::operator()(const Object::Move& move, const Object_Ptr& collider, const Object_Ptr& collidee,
                                                               const PlayingArea& playingArea, const ObjectDatabase& objectDatabase) const
{
  // Kill the collidee.
  collidee->kill();

  // Provided the collider and the collidee are not the same object, allow the move; otherwise, prevent it.
  return collider != collidee ? boost::optional<Object::Move>(move) : boost::none;
}

}
