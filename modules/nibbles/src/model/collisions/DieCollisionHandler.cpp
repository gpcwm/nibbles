/**
 * nibbles: DieCollisionHandler.cpp
 */

#include "model/collisions/DieCollisionHandler.h"

namespace nibbles {

//#################### PRIVATE OPERATORS ####################

boost::optional<Object::Move> DieCollisionHandler::operator()(const Object::Move& move, const Object_Ptr& collider, const Object_Ptr& collidee,
                                                              const PlayingArea& playingArea, const ObjectDatabase& objectDatabase) const
{
  // Kill the collider.
  collider->kill();

  // Prevent the move.
  return boost::none;
}

}
