/**
 * nibbles: ConsumeCollisionHandler.cpp
 */

#include "model/collisions/ConsumeCollisionHandler.h"

namespace nibbles {

//#################### PRIVATE OPERATORS ####################

boost::optional<Object::Move> ConsumeCollisionHandler::operator()(const Object::Move& move, const Object_Ptr& collider, const Object_Ptr& collidee,
                                                                  const PlayingArea& playingArea, const ObjectDatabase& objectDatabase) const
{
  // Kill the collidee.
  collidee->kill();

  // Suppress the removal of any cells from the collider.
  return Object::Move(move.objectID, move.cellToAdd, std::vector<std::pair<int,int> >());
}

}
