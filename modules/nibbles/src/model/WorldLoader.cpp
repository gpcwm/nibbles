/**
 * nibbles: WorldLoader.cpp
 */

#include "model/WorldLoader.h"

#include <fstream>
#include <stdexcept>

#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/tokenizer.hpp>

#include "model/objects/Snake.h"
#include "model/objects/StaticObject.h"

namespace nibbles {

//#################### PUBLIC STATIC MEMBER FUNCTIONS ####################

World_Ptr WorldLoader::load_world(const std::string& filename)
try
{
  World_Ptr world;

  // Attempt to open the world file.
  std::ifstream fs(filename.c_str());
  if(!fs) throw std::runtime_error("Could not open '" + filename + "' for reading");

  // Read the world in line by line.
  int stage = 0, i = 0;
  std::vector<std::pair<int,int> > wallCells;
  std::string line;
  while(std::getline(fs, line))
  {
    switch(stage)
    {
      case 0: // read the playing area size
      {
        boost::tokenizer<> tok(line);
        std::vector<std::string> tokens(tok.begin(), tok.end());
        if(tokens.size() != 2) throw std::runtime_error("Bad playing area size whilst trying to read world from '" + filename + "'");

        int playingAreaHeight = boost::lexical_cast<int>(tokens[0]);
        int playingAreaWidth = boost::lexical_cast<int>(tokens[1]);
        world.reset(new World(playingAreaHeight, playingAreaWidth));

        stage = 1;

        break;
      }
      case 1: // read the playing area
      {
        if(line.length() != world->playing_area().width())
        {
          throw std::runtime_error("Playing area line " + boost::lexical_cast<std::string>(i) + " has the wrong width");
        }

        for(int j = 0; j < static_cast<int>(line.length()); ++j)
        {
          if(boost::lexical_cast<int>(line[j]) == 1)
          {
            wallCells.push_back(std::make_pair(i, j));
          }
        }

        if(++i == world->playing_area().height())
        {
          world->add_object(Object_Ptr(new StaticObject("Wall", wallCells)));
          stage = 2;
        }

        break;
      }
      case 2: // read the objects
      {
        boost::tokenizer<> tok(line);
        std::vector<std::string> tokens(tok.begin(), tok.end());
        if(tokens.empty()) break;

        if(tokens[0] == "Snake")
        {
          std::vector<std::pair<int,int> > cells = extract_cells(tokens, 1, static_cast<int>(tokens.size()) - 3, filename);

          boost::optional<Snake::Direction> dir;
          std::string dirString = tokens[tokens.size() - 1];
          if(dirString == "DOWN") dir = Snake::DOWN;
          if(dirString == "LEFT") dir = Snake::LEFT;
          if(dirString == "RIGHT") dir = Snake::RIGHT;
          if(dirString == "UP") dir = Snake::UP;
          if(!dir) throw std::runtime_error("Missing snake direction whilst trying to read world from '" + filename + "'");

          world->add_object(Object_Ptr(new Snake(cells, *dir)));
        }
        else if(tokens[0] == "StaticObject")
        {
          if(tokens.size() < 3) throw std::runtime_error("Bad static object whilst trying to read world from '" + filename + "'");

          std::string type = tokens[1];
          std::vector<std::pair<int,int> > cells = extract_cells(tokens, 2, static_cast<int>(tokens.size()) - 2, filename);

          world->add_object(Object_Ptr(new StaticObject(type, cells)));
        }

        break;
      }
    }
  }

  // If we didn't even manage to read the playing area, something's gone wrong.
  if(stage < 2) throw std::runtime_error("Unexpected end of file whilst trying to read world from '" + filename + "'");

  return world;
}
catch(std::exception& e)
{
  throw std::runtime_error(e.what());
}

//#################### PRIVATE STATIC MEMBER FUNCTIONS ####################

std::vector<std::pair<int,int> > WorldLoader::extract_cells(const std::vector<std::string>& tokens, int first, int last, const std::string& filename)
{
  std::vector<std::pair<int,int> > cells;

  for(size_t k = first; k <= last; k += 2)
  {
    cells.push_back(std::make_pair(boost::lexical_cast<int>(tokens[k]), boost::lexical_cast<int>(tokens[k+1])));
  }

  if(cells.empty()) throw std::runtime_error("Missing cells whilst trying to read world from '" + filename + "'");

  return cells;
}

}
