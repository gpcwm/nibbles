/**
 * nibbles: Snake.cpp
 */

#include "model/objects/Snake.h"

#include <stdexcept>

#include <boost/assign/list_of.hpp>
using boost::assign::list_of;

namespace nibbles {

//#################### CONSTRUCTORS ####################

Snake::Snake(const std::vector<std::pair<int,int> >& cells, Direction dir)
: Object(static_type()),
  m_cells(cells.begin(), cells.end()),
  m_dir(dir)
{}

Snake::Snake(const boost::assign_detail::generic_list<std::pair<int,int> >& cells, Direction dir)
: Object(static_type()),
  m_cells(cells.begin(), cells.end()),
  m_dir(dir)
{}

//#################### PUBLIC MEMBER FUNCTIONS ####################

void Snake::apply_move(const Move& move, PlayingArea& playingArea)
{
  if(move.objectID != id())
  {
    // This should never happen.
    throw std::runtime_error("Trying to apply a move to the wrong object");
  }

  if(move.cellToAdd)
  {
    m_cells.push_front(*move.cellToAdd);
    playingArea.cell(move.cellToAdd->first, move.cellToAdd->second) = id();
  }

  for(size_t k = 0, size = move.cellsToRemove.size(); k < size; ++k)
  {
    if(m_cells.back() != move.cellsToRemove[k])
    {
      throw std::runtime_error("Trying to remove a cell that is not at the end of the snake");
    }

    m_cells.pop_back();
    playingArea.cell(move.cellsToRemove[k].first, move.cellsToRemove[k].second) = 0;
  }
}

boost::optional<Snake::Move> Snake::choose_move(const PlayingArea& playingArea) const
{
  std::pair<int,int> head = m_cells.front();
  std::pair<int,int> newHead;
  switch(m_dir)
  {
    case DOWN:  newHead = std::make_pair(head.first + 1, head.second); break;
    case LEFT:  newHead = std::make_pair(head.first, head.second - 1); break;
    case RIGHT: newHead = std::make_pair(head.first, head.second + 1); break;
    default:    newHead = std::make_pair(head.first - 1, head.second); break;
  }
  return Snake::Move(id(), newHead, list_of(m_cells.back()));
}

const std::deque<std::pair<int,int> >& Snake::get_cells() const
{
  return m_cells;
}

int Snake::length() const
{
  return static_cast<int>(m_cells.size());
}

void Snake::set_direction(Direction dir)
{
  m_dir = dir;
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

std::vector<std::pair<int,int> > Snake::cells() const
{
  return std::vector<std::pair<int,int> >(m_cells.begin(), m_cells.end());
}

//#################### PUBLIC STATIC MEMBER FUNCTIONS ####################

std::string Snake::static_type()
{
  return "Snake";
}

}
