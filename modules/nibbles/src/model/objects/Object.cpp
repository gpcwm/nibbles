/**
 * nibbles: Object.cpp
 */

#include "model/objects/Object.h"

namespace nibbles {

//#################### CONSTRUCTORS ####################

Object::Object(const std::string& type)
: m_alive(true), m_id(-1), m_type(type)
{}

//#################### DESTRUCTOR ####################

Object::~Object() {}

//#################### PUBLIC MEMBER FUNCTIONS ####################

void Object::add_to_playing_area(PlayingArea& playingArea) const
{
  std::vector<std::pair<int,int> > objectCells = cells();
  for(size_t k = 0, size = objectCells.size(); k < size; ++k)
  {
    int i = objectCells[k].first, j = objectCells[k].second;
    playingArea.cell(i, j) = m_id;
  }
}

bool Object::alive() const
{
  return m_alive;
}

void Object::apply_move(const Move& move, PlayingArea& playingArea)
{
  // By default, objects just stay where they are.
}

bool Object::can_add_to_playing_area(const PlayingArea& playingArea) const
{
  std::vector<std::pair<int,int> > objectCells = cells();
  for(size_t k = 0, size = objectCells.size(); k < size; ++k)
  {
    int i = objectCells[k].first, j = objectCells[k].second;
    if(playingArea.cell(i, j) != 0) return false;
  }
  return true;
}

boost::optional<Object::Move> Object::choose_move(const PlayingArea& playingArea) const
{
  // By default, objects just stay where they are.
  return boost::none;
}

int Object::id() const
{
  return m_id;
}

void Object::kill()
{
  m_alive = false;
}

void Object::remove_from_playing_area(PlayingArea& playingArea) const
{
  std::vector<std::pair<int,int> > objectCells = cells();
  for(size_t k = 0, size = objectCells.size(); k < size; ++k)
  {
    int i = objectCells[k].first, j = objectCells[k].second;
    playingArea.cell(i, j) = 0;
  }
}

const std::string& Object::type() const
{
  return m_type;
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

void Object::set_id(int id)
{
  m_id = id;
}

}
