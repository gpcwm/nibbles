/**
 * nibbles: ObjectDatabase.cpp
 */

#include "model/objects/ObjectDatabase.h"

namespace nibbles {

//#################### CONSTRUCTORS ####################

ObjectDatabase::ObjectDatabase()
{
  // Allocate an ID to represent empty cells.
  m_idAllocator.allocate();
}

//#################### PUBLIC MEMBER FUNCTIONS ####################

void ObjectDatabase::add_object(const Object_Ptr& object)
{
  int id = m_idAllocator.allocate();
  object->set_id(id);
  m_objects.insert(std::make_pair(id, object));
}

Object_Ptr ObjectDatabase::object(int objectID) const
{
  std::map<int,Object_Ptr>::const_iterator it = m_objects.find(objectID);
  return it != m_objects.end() ? it->second : Object_Ptr();
}

const std::map<int,Object_Ptr>& ObjectDatabase::objects() const
{
  return m_objects;
}

void ObjectDatabase::remove_object(const Object_Ptr& object)
{
  m_idAllocator.deallocate(object->id());
  m_objects.erase(object->id());
}

void ObjectDatabase::sweep()
{
  for(std::map<int,Object_Ptr>::iterator it = m_objects.begin(), iend = m_objects.end(); it != iend; /* No-op */)
  {
    if(!it->second->alive())
    {
      m_idAllocator.deallocate(it->second->id());
      it->second->set_id(-1);
      m_objects.erase(it++);
    }
    else ++it;
  }
}

}
