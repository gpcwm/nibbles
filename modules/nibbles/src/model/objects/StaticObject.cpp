/**
 * nibbles: StaticObject.cpp
 */

#include "model/objects/StaticObject.h"

namespace nibbles {

//#################### CONSTRUCTORS ####################

StaticObject::StaticObject(const std::string& type, const std::vector<std::pair<int,int> >& cells)
: Object(type), m_cells(cells.begin(), cells.end())
{}

StaticObject::StaticObject(const std::string& type, const boost::assign_detail::generic_list<std::pair<int,int> >& cells)
: Object(type), m_cells(cells.begin(), cells.end())
{}

//#################### PUBLIC MEMBER FUNCTIONS ####################

void StaticObject::remove_cell(const std::pair<int,int>& cell, PlayingArea& playingArea)
{
  m_cells.erase(cell);
  playingArea.cell(cell.first, cell.second) = 0;
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

std::vector<std::pair<int,int> > StaticObject::cells() const
{
  return std::vector<std::pair<int,int> >(m_cells.begin(), m_cells.end());
}

}
