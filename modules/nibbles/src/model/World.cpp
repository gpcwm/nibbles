/**
 * nibbles: World.cpp
 */

#include "model/World.h"

namespace nibbles {

//#################### CONSTRUCTORS ####################

World::World(int playingAreaHeight, int playingAreaWidth)
: m_playingArea(playingAreaHeight, playingAreaWidth)
{}

//#################### PUBLIC MEMBER FUNCTIONS ####################

bool World::add_object(const Object_Ptr& object)
{
  if(!object->can_add_to_playing_area(m_playingArea)) return false;
  m_objectDatabase.add_object(object);
  object->add_to_playing_area(m_playingArea);
  return true;
}

const ObjectDatabase& World::object_database() const
{
  return m_objectDatabase;
}

PlayingArea& World::playing_area()
{
  return m_playingArea;
}

const PlayingArea& World::playing_area() const
{
  return m_playingArea;
}

void World::remove_object(const Object_Ptr& object)
{
  m_objectDatabase.remove_object(object);
  object->remove_from_playing_area(m_playingArea);
}

void World::run_step()
{
  // For each object in the database:
  {
    // If the object is no longer alive (i.e. it has been killed this frame), skip over it.

    // Get the move that the object wants to make (if any).

    // If moving the object would cause a collision:
    {
      // Call the appropriate collision handler.

      // Remove dead objects from the playing area.

      // If the collision handler cancelled the move, continue on to the next object.
    }

    // Move the object.
  }

  // Remove dead objects from the object database.
}

void World::set_collision_handler(const std::pair<std::string,std::string>& objectTypes, const CollisionHandler_CPtr& collisionHandler)
{
  m_collisionHandlers[objectTypes] = collisionHandler;
}

}
