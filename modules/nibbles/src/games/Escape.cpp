/**
 * nibbles: Escape.cpp
 */

#include "games/Escape.h"

#include <boost/assign/list_of.hpp>
using boost::assign::list_of;

#include <tvginput/InputState.h>
using namespace tvginput;

#include "model/collisions/ConsumeCollisionHandler.h"
#include "model/collisions/DieCollisionHandler.h"
#include "model/collisions/KillCollisionHandler.h"

namespace nibbles {

//#################### CONSTRUCTORS ####################

Escape::Escape(const std::string& worldFilename)
: Game(worldFilename)
{
  // Get typed pointers to important objects in the world.
  const ObjectDatabase& objDatabase = m_world->object_database();
  m_walls = boost::dynamic_pointer_cast<StaticObject>(objDatabase.object(1));
  m_snake = boost::dynamic_pointer_cast<Snake>(objDatabase.object(2));

  // Set up the collision handlers for the world.
  m_world->set_collision_handler(std::make_pair("Snake", "Apple"), CollisionHandler_CPtr(new ConsumeCollisionHandler));
  m_world->set_collision_handler(std::make_pair("Snake", "Exit"), CollisionHandler_CPtr(new KillCollisionHandler));
  m_world->set_collision_handler(std::make_pair("Snake", "Snake"), CollisionHandler_CPtr(new KillCollisionHandler));
  m_world->set_collision_handler(std::make_pair("Snake", "Wall"), CollisionHandler_CPtr(new DieCollisionHandler));
}

//#################### PUBLIC MEMBER FUNCTIONS ####################

std::string Escape::outcome_message() const
{
  return m_outcomeMessage;
}

void Escape::process_input(const InputState& inputState)
{
  if(inputState.key_down(KEYCODE_DOWN)) m_snake->set_direction(Snake::DOWN);
  if(inputState.key_down(KEYCODE_LEFT)) m_snake->set_direction(Snake::LEFT);
  if(inputState.key_down(KEYCODE_RIGHT)) m_snake->set_direction(Snake::RIGHT);
  if(inputState.key_down(KEYCODE_UP)) m_snake->set_direction(Snake::UP);
}

std::string Escape::type() const
{
  return "Escape";
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

void Escape::run_step_post()
{
  // Every few time steps, randomly add an apple to an empty cell in the world.
  if(m_timestamp % 5 == 4)
  {
    randomly_add_static_object("Apple");
  }

  // If the snake is long enough and the exit has not yet been spawned, spawn it.
  if(m_snake->length() >= 5 && !m_exit)
  {
    // FIXME: The exit cell should be generated randomly.
    std::pair<int,int> exitCell(0, 1);
    m_walls->remove_cell(exitCell, m_world->playing_area());
    m_exit.reset(new StaticObject("Exit", list_of(exitCell)));
    m_world->add_object(m_exit);
  }

  // If the snake dies, the player loses.
  if(!m_snake->alive()) m_outcomeMessage = "You Lose!";

  // If the snake reaches the exit, the player wins.
  if(m_exit && !m_exit->alive()) m_outcomeMessage = "You Win!";
}

}
