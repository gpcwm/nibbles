/**
 * nibbles: Game.cpp
 */

#include "games/Game.h"

#include <boost/assign/list_of.hpp>
using boost::assign::list_of;

#include "model/WorldLoader.h"
#include "model/objects/StaticObject.h"
using namespace tvginput;

namespace nibbles {

//#################### CONSTRUCTORS ####################

Game::Game(const std::string& worldFilename)
: m_timestamp(0), m_world(WorldLoader::load_world(worldFilename))
{}

//#################### DESTRUCTOR ####################

Game::~Game() {}

//#################### PUBLIC MEMBER FUNCTIONS ####################

std::string Game::outcome_message() const
{
  // Games continue forever by default.
  return "";
}

void Game::process_input(const InputState& inputState)
{
  // No-op by default.
}

void Game::run_step()
{
  if(!m_world) return;
  run_step_pre();
  m_world->run_step();
  run_step_post();
  ++m_timestamp;
}

World_CPtr Game::world() const
{
  return m_world;
}

//#################### PROTECTED MEMBER FUNCTIONS ####################

void Game::randomly_add_static_object(const std::string& type)
{
  // Make a vector containing all of the empty cells in the world.
  std::vector<std::pair<int,int> > emptyCells;
  const PlayingArea& playingArea = m_world->playing_area();
  for(int i = 0, height = playingArea.height(); i < height; ++i)
  {
    for(int j = 0, width = playingArea.width(); j < width; ++j)
    {
      if(playingArea.cell(i, j) == 0) emptyCells.push_back(std::make_pair(i, j));
    }
  }

  // Add a static object of the specified type to a randomly-chosen empty cell.
  m_world->add_object(Object_Ptr(new StaticObject(type, list_of(emptyCells[rand() % emptyCells.size()]))));
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

void Game::run_step_post()
{
  // No-op by default
}

void Game::run_step_pre()
{
  // No-op by default
}

//#################### PROTECTED STATIC MEMBER FUNCTIONS ####################

std::vector<std::pair<int,int> > Game::mask_to_cells(const boost::assign_detail::generic_list<int>& mask) const
{
  std::vector<std::pair<int,int> > result;

  int width = m_world->playing_area().width();
  std::vector<int> maskv(mask.begin(), mask.end());

  for(int k = 0, size = static_cast<int>(maskv.size()); k < size; ++k)
  {
    if(!maskv[k]) continue;

    int i = k / width, j = k % width;
    result.push_back(std::make_pair(i, j));
  }

  return result;
}

}
