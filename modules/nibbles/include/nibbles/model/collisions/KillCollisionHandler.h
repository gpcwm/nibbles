/**
 * nibbles: KillCollisionHandler.h
 */

#ifndef H_NIBBLES_KILLCOLLISIONHANDLER
#define H_NIBBLES_KILLCOLLISIONHANDLER

#include "CollisionHandler.h"

namespace nibbles {

/**
 * \brief An instance of this class can be used to handle collisions in a Nibbles world in which the collider kills the collidee.
 */
class KillCollisionHandler : public TypedCollisionHandler<Object,Object>
{
  //#################### PRIVATE OPERATORS ####################
private:
  /** Override */
  virtual boost::optional<Object::Move> operator()(const Object::Move& move, const Object_Ptr& collider, const Object_Ptr& collidee,
                                                   const PlayingArea& playingArea, const ObjectDatabase& objectDatabase) const;
};

}

#endif
