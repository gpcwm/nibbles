/**
 * nibbles: CollisionHandler.h
 */

#ifndef H_NIBBLES_COLLISIONHANDLER
#define H_NIBBLES_COLLISIONHANDLER

#include "../base/PlayingArea.h"
#include "../objects/ObjectDatabase.h"

namespace nibbles {

/**
 * \brief An instance of a class deriving from this one can be used to handle object-object collisions in a Nibbles world.
 */
class CollisionHandler
{
  //#################### DESTRUCTOR ####################
public:
  /**
   * \brief Destroys the collision handler.
   */
  virtual ~CollisionHandler() {}

  //#################### PUBLIC ABSTRACT OPERATORS ####################
public:
  /**
   * \brief Handles a collision that would be caused by the movement of an object in a Nibbles world.
   *
   * \param move            The move that would cause a collision.
   * \param playingArea     The playing area for the Nibbles world.
   * \param objectDatabase  The object database for the Nibbles world.
   * \return                An optional new move after resolving the collision.
   */
  virtual boost::optional<Object::Move> operator()(const Object::Move& move, const PlayingArea& playingArea, const ObjectDatabase& objectDatabase) const = 0;
};

/**
 * \brief An instance of a class deriving from this class template can be used to handle typed object-object collisions in a Nibbles world.
 */
template <typename ColliderType, typename CollideeType>
class TypedCollisionHandler : public CollisionHandler
{
  //#################### PUBLIC OPERATORS ####################
public:
  /** Override */
  virtual boost::optional<Object::Move> operator()(const Object::Move& move, const PlayingArea& playingArea, const ObjectDatabase& objectDatabase) const
  {
    Object_Ptr collider = objectDatabase.object(move.objectID);
    Object_Ptr collidee = objectDatabase.object(playingArea.cell(move.cellToAdd->first, move.cellToAdd->second));
    return operator()(move, boost::dynamic_pointer_cast<ColliderType>(collider), boost::dynamic_pointer_cast<CollideeType>(collidee), playingArea, objectDatabase);
  }

  //#################### PRIVATE ABSTRACT OPERATORS ####################
private:
  /**
   * \brief Handles a collision that would be caused by the movement of an object in a Nibbles world.
   *
   * \param move            The move that would cause a collision.
   * \param collider        The object causing the collision.
   * \param collidee        The object with which the collision is occurring.
   * \param playingArea     The playing area for the Nibbles world.
   * \param objectDatabase  The object database for the Nibbles world.
   * \return                An optional new move after resolving the collision.
   */
  virtual boost::optional<Object::Move> operator()(const Object::Move& move, const boost::shared_ptr<ColliderType>& collider, const boost::shared_ptr<CollideeType>& collidee,
                                                   const PlayingArea& playingArea, const ObjectDatabase& objectDatabase) const = 0;
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<const CollisionHandler> CollisionHandler_CPtr;

}

#endif
