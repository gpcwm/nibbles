/**
 * nibbles: WorldLoader.h
 */

#ifndef H_NIBBLES_WORLDLOADER
#define H_NIBBLES_WORLDLOADER

#include "World.h"

namespace nibbles {

/**
 * \brief This class can be used to load Nibbles game worlds.
 */
class WorldLoader
{
  //#################### PUBLIC STATIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Loads a Nibbles game world from the specified file.
   *
   * \param filename            The name of the world file from which to load.
   * \return                    The loaded game world.
   * \throws std::runtime_error If the contents of the world file are invalid.
   */
  static World_Ptr load_world(const std::string& filename);

  //#################### PRIVATE STATIC MEMBER FUNCTIONS ####################
private:
  /**
   * \brief TODO
   */
  static std::vector<std::pair<int,int> > extract_cells(const std::vector<std::string>& tokens, int first, int last, const std::string& filename);
};

}

#endif
