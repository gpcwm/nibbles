/**
 * nibbles: World.h
 */

#ifndef H_NIBBLES_WORLD
#define H_NIBBLES_WORLD

#include "base/PlayingArea.h"
#include "collisions/CollisionHandler.h"
#include "objects/ObjectDatabase.h"

namespace nibbles {

/**
 * \brief An instance of this class represents a Nibbles world.
 */
class World
{
  //#################### PRIVATE VARIABLES ####################
private:
  /** The collision handlers for the world. */
  std::map<std::pair<std::string,std::string>,CollisionHandler_CPtr> m_collisionHandlers;

  /** The object database for the world. */
  ObjectDatabase m_objectDatabase;

  /** The playing area for the world. */
  PlayingArea m_playingArea;

  //#################### CONSTRUCTORS ####################
public:
  /**
   * \brief Constructs a Nibbles world.
   *
   * \param playingAreaHeight The height of the playing area for the world (in cells).
   * \param playingAreaWidth  The width of the playing area for the world (in cells).
   */
  World(int playingAreaHeight, int playingAreaWidth);

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Attempts to add an object to the world.
   *
   * \param object  The object to add.
   * \return        true, if the operation succeeded, or false otherwise.
   */
  bool add_object(const Object_Ptr& object);

  /**
   * \brief Gets the object database for the world.
   *
   * \return  The object database for the world.
   */
  const ObjectDatabase& object_database() const;

  /**
   * \brief Gets the playing area for the world.
   *
   * \return  The playing area for the world.
   */
  PlayingArea& playing_area();

  /**
   * \brief Gets the playing area for the world.
   *
   * \return  The playing area for the world.
   */
  const PlayingArea& playing_area() const;

  /**
   * \brief Removes an object from the world.
   *
   * \param object  The object to remove.
   */
  void remove_object(const Object_Ptr& object);

  /**
   * \brief Runs the simulator for a single step.
   */
  void run_step();

  /**
   * \brief Sets the collision handler for a collision between two objects of the specified types.
   *
   * \param objectTypes       The types of objects that are colliding (collider first, then collidee).
   * \param collisionHandler  The collision handler to use for a collision between those types of objects.
   */
  void set_collision_handler(const std::pair<std::string,std::string>& objectTypes, const CollisionHandler_CPtr& collisionHandler);
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<World> World_Ptr;
typedef boost::shared_ptr<const World> World_CPtr;

}

#endif
