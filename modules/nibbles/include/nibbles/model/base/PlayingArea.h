/**
 * nibbles: PlayingArea.h
 */

#ifndef H_NIBBLES_PLAYINGAREA
#define H_NIBBLES_PLAYINGAREA

#include <vector>

#include <boost/shared_ptr.hpp>

namespace nibbles {

/**
 * \brief An instance of this class can be used to represent the playing area in a Nibbles world.
 */
class PlayingArea
{
  //#################### PRIVATE VARIABLES ####################
private:
  /** The cells that make up the playing area. Each cell stores a value (0 = empty, anything else = object index). */
  std::vector<int> m_cells;

  /** The height of the playing area (in cells). */
  int m_height;

  /** The width of the playing area (in cells). */
  int m_width;

  //#################### CONSTRUCTORS ####################
public:
  /**
   * \brief Constructs an empty playing area.
   *
   * \param height  The height of the playing area (in cells).
   * \param width   The width of the playing area (in cells).
   */
  PlayingArea(int height, int width);

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Gets a reference to a cell.
   *
   * \param i The row index of the cell.
   * \param j The column index of the cell.
   * \return  A reference to the cell.
   */
  int& cell(int i, int j);

  /**
   * \brief Gets the value of a cell.
   *
   * \param i                   The row index of the cell.
   * \param j                   The column index of the cell.
   * \return                    The value of the cell.
   * \throws std::runtime_error If the cell location is out of bounds.
   */
  int cell(int i, int j) const;

  /**
   * \brief Gets the height of the playing area (in cells).
   *
   * \return  The height of the playing area (in cells).
   */
  int height() const;

  /**
   * \brief Gets the width of the playing area (in cells).
   *
   * \return  The width of the playing area (in cells).
   */
  int width() const;

  //#################### PRIVATE MEMBER FUNCTIONS ####################
private:
  /**
   * \brief Ensures that the specified (i,j) coordinates denote a cell that is within the playing area.
   *
   * \brief i                   The row index of the cell.
   * \brief j                   The column index of the cell.
   * \throws std::runtime_error If the denoted cell is outside the playing area.
   */
  void ensure_bounds(int i, int j) const;
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<PlayingArea> PlayingArea_Ptr;
typedef boost::shared_ptr<const PlayingArea> PlayingArea_CPtr;

}

#endif
