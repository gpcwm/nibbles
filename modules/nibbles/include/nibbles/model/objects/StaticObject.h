/**
 * nibbles: StaticObject.h
 */

#ifndef H_NIBBLES_STATICOBJECT
#define H_NIBBLES_STATICOBJECT

#include <set>

#include <boost/assign/list_of.hpp>

#include "Object.h"

namespace nibbles {

/**
 * \brief An instance of this class represents a static object (e.g. a wall) in a Nibbles world.
 */
class StaticObject : public Object
{
  //#################### PRIVATE VARIABLES ####################
private:
  /** The playing area cells occupied by the object. */
  std::set<std::pair<int,int> > m_cells;

  //#################### CONSTRUCTORS ####################
public:
  /**
   * \brief Constructs a static object.
   *
   * \param type  The type of the object.
   * \param cells The playing area cells occupied by the object.
   */
  StaticObject(const std::string& type, const std::vector<std::pair<int,int> >& cells);

  /**
   * \brief Constructs a static object.
   *
   * \param type  The type of the object.
   * \param cells The playing area cells occupied by the object.
   */
  StaticObject(const std::string& type, const boost::assign_detail::generic_list<std::pair<int,int> >& cells);

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Removes a cell from the object.
   *
   * \param cell        The cell to remove.
   * \param playingArea The playing area containing the object.
   */
  void remove_cell(const std::pair<int,int>& cell, PlayingArea& playingArea);

  //#################### PRIVATE MEMBER FUNCTIONS ####################
private:
  /** Override */
  virtual std::vector<std::pair<int,int> > cells() const;
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<StaticObject> StaticObject_Ptr;

}

#endif
