/**
 * nibbles: ObjectDatabase.h
 */

#ifndef H_NIBBLES_OBJECTDATABASE
#define H_NIBBLES_OBJECTDATABASE

#include <map>

#include "Object.h"
#include "../../util/IDAllocator.h"

namespace nibbles {

/**
 * \brief An instance of this class represents a database of the objects in a Nibbles world.
 */
class ObjectDatabase
{
  //#################### PRIVATE VARIABLES ####################
private:
  /** An ID allocator for the objects. */
  IDAllocator m_idAllocator;

  /** The objects in the world (represented as a map from IDs to objects). */
  std::map<int,Object_Ptr> m_objects;

  //#################### CONSTRUCTORS ####################
public:
  /**
   * \brief Constructs an object database.
   */
  ObjectDatabase();

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Adds an object to the database.
   *
   * \param object  The object to add.
   */
  void add_object(const Object_Ptr& object);

  /**
   * \brief Gets the object with the specified ID (if it exists).
   *
   * \param objectID  The ID of the object to get.
   * \return          The object (if it exists), or null otherwise.
   */
  Object_Ptr object(int objectID) const;

  /**
   * \brief Gets the objects in the world.
   *
   * \return  The objects in the world.
   */
  const std::map<int,Object_Ptr>& objects() const;

  /**
   * \brief Removes an object from the database.
   *
   * \param object  The object to remove.
   */
  void remove_object(const Object_Ptr& object);

  /**
   * \brief Removes dead objects from the database.
   */
  void sweep();
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<ObjectDatabase> ObjectDatabase_Ptr;
typedef boost::shared_ptr<const ObjectDatabase> ObjectDatabase_CPtr;

}

#endif
