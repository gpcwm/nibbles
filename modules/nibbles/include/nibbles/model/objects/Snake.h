/**
 * nibbles: Snake.h
 */

#ifndef H_NIBBLES_SNAKE
#define H_NIBBLES_SNAKE

#include <deque>

#include <boost/assign/list_of.hpp>

#include "Object.h"

namespace nibbles {

/**
 * \brief An instance of this class represents a snake in a Nibbles world.
 */
class Snake : public Object
{
  //#################### ENUMERATIONS ####################
public:
  /**
   * \brief The values of this enumeration denote the different directions in which the snake can move.
   */
  enum Direction
  {
    DOWN,
    LEFT,
    RIGHT,
    UP,
  };

  //#################### PRIVATE VARIABLES ####################
private:
  /** The playing area cells occupied by the snake. */
  std::deque<std::pair<int,int> > m_cells;

  /** The direction in which the snake should move. */
  Direction m_dir;

  //#################### CONSTRUCTORS ####################
public:
  /**
   * \brief Constructs a snake.
   *
   * \param cells The playing area cells occupied by the snake.
   * \param dir   The direction in which the snake should move.
   */
  Snake(const std::vector<std::pair<int,int> >& cells, Direction dir);

  /**
   * \brief Constructs a snake.
   *
   * \param cells The playing area cells occupied by the snake.
   * \param dir   The direction in which the snake should move.
   */
  Snake(const boost::assign_detail::generic_list<std::pair<int,int> >& cells, Direction dir);

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /** Override */
  virtual void apply_move(const Move& move, PlayingArea& playingArea);

  /** Override */
  virtual boost::optional<Move> choose_move(const PlayingArea& playingArea) const;

  /**
   * \brief Gets the playing area cells occupied by the snake.
   *
   * \return  The playing area cells occupied by the snake.
   */
  const std::deque<std::pair<int,int> >& get_cells() const;

  /**
   * \brief Gets the length of the snake.
   *
   * \return  The length of the snake.
   */
  int length() const;

  /**
   * \brief Sets the direction in which the snake should move.
   *
   * \param dir The direction in which the snake should move.
   */
  void set_direction(Direction dir);

  //#################### PRIVATE MEMBER FUNCTIONS ####################
private:
  /** Override */
  virtual std::vector<std::pair<int,int> > cells() const;

  //#################### PUBLIC STATIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Returns "Snake", the type string for Snake objects.
   *
   * \return  The type string for Snake objects.
   */
  static std::string static_type();
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<Snake> Snake_Ptr;

}

#endif
