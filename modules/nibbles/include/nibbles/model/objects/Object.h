/**
 * nibbles: Object.h
 */

#ifndef H_NIBBLES_OBJECT
#define H_NIBBLES_OBJECT

#include <string>
#include <vector>

#include <boost/optional.hpp>

#include "../base/PlayingArea.h"

namespace nibbles {

/**
 * \brief An instance of a class deriving from this one represents a game object in a Nibbles world, e.g. the wall or a snake.
 */
class Object
{
  //#################### NESTED TYPES ####################
public:
  /**
   * \brief An instance of this struct represents the movement of an object in a Nibbles world.
   */
  struct Move
  {
    //~~~~~~~~~~~~~~~~~~~~ PUBLIC VARIABLES ~~~~~~~~~~~~~~~~~~~~

    /** An optional playing area cell that will be newly occupied by the object. */
    boost::optional<std::pair<int,int> > cellToAdd;

    /** The playing area cells that will no longer be occupied by the object. */
    std::vector<std::pair<int,int> > cellsToRemove;

    /** The ID of the object. */
    int objectID;

    //~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORS ~~~~~~~~~~~~~~~~~~~~

    template <typename T>
    Move(int objectID_, const boost::optional<std::pair<int,int> >& cellToAdd_, const T& cellsToRemove_)
    : cellToAdd(cellToAdd_),
      cellsToRemove(cellsToRemove_.begin(), cellsToRemove_.end()),
      objectID(objectID_)
    {}
  };

  //#################### PRIVATE VARIABLES ####################
private:
  /** Whether or not the object is alive. */
  bool m_alive;

  /** The ID of the object. */
  int m_id;

  /** The type of the object. */
  std::string m_type;

  //#################### CONSTRUCTORS ####################
protected:
  /**
   * \brief Constructs an object.
   *
   * \param type  The type of the object.
   */
  explicit Object(const std::string& type);

  //#################### DESTRUCTOR ####################
public:
  /**
   * \brief Destroys the object.
   */
  virtual ~Object();

  //#################### PRIVATE ABSTRACT MEMBER FUNCTIONS ####################
private:
  /**
   * \brief Gets the playing area cells occupied by the object.
   *
   * \return  The playing area cells occupied by the object.
   */
  virtual std::vector<std::pair<int,int> > cells() const = 0;

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Adds the object to a playing area.
   *
   * \pre               can_add_to_playing_area(playingArea)
   * \param playingArea The playing area to which to add the object.
   */
  void add_to_playing_area(PlayingArea& playingArea) const;

  /**
   * \brief Gets whether or not the object is alive.
   *
   * \return  true, if the object is alive, or false otherwise.
   */
  bool alive() const;

  /**
   * \brief Applies the specified move to the object.
   *
   * \param move        The move to apply.
   * \param playingArea The playing area containing the object.
   */
  virtual void apply_move(const Move& move, PlayingArea& playingArea);

  /**
   * \brief Checks whether the object can be added to a playing area.
   *
   * \param playingArea The playing area to check against.
   * \return            true, if the object can be added to the playing area, or false otherwise.
   */
  bool can_add_to_playing_area(const PlayingArea& playingArea) const;

  /**
   * \brief Chooses an optional move for the object to make.
   *
   * The actual outcome of the move will need to be resolved during simulation.
   *
   * \param playingArea The playing area containing the object.
   * \return            An optional move for the object to make.
   */
  virtual boost::optional<Move> choose_move(const PlayingArea& playingArea) const;

  /**
   * \brief Gets the ID of the object.
   *
   * \return  The ID of the object.
   */
  int id() const;

  /**
   * \brief Kills the object (sets its alive flag to false).
   */
  void kill();

  /**
   * \brief Removes the object from its playing area.
   *
   * \param playingArea The playing area containing the object.
   */
  void remove_from_playing_area(PlayingArea& playingArea) const;

  /**
   * \brief Gets the type of the object.
   *
   * \return  The type of the object.
   */
  const std::string& type() const;

  //#################### PRIVATE MEMBER FUNCTIONS ####################
private:
  /**
   * \brief Sets the ID of the object.
   */
  void set_id(int id);

  //#################### FRIENDS ####################

  friend class ObjectDatabase;
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<Object> Object_Ptr;
typedef boost::shared_ptr<const Object> Object_CPtr;

}

#endif
