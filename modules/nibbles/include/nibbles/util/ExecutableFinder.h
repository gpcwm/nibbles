/**
 * nibbles: ExecutableFinder.h
 */

#ifndef H_NIBBLES_EXECUTABLEFINDER
#define H_NIBBLES_EXECUTABLEFINDER

#include <boost/filesystem.hpp>

namespace nibbles {

/**
 * \brief Finds the path to the current executable.
 *
 * \return  The path to the current executable.
 */
extern boost::filesystem::path find_executable();

}

#endif
