/**
 * nibbles: Escape.h
 */

#ifndef H_NIBBLES_ESCAPE
#define H_NIBBLES_ESCAPE

#include "Game.h"
#include "../model/objects/Snake.h"
#include "../model/objects/StaticObject.h"

namespace nibbles {

/**
 * \brief An instance of this class represents a one-player Nibbles escape game.
 */
class Escape : public Game
{
  //#################### PRIVATE VARIABLES ####################
private:
  /** The exit. */
  StaticObject_Ptr m_exit;

  /** A message indicating the outcome of the game (empty while the game is in progress). */
  std::string m_outcomeMessage;

  /** The snake. */
  Snake_Ptr m_snake;

  /** The walls. */
  StaticObject_Ptr m_walls;

  //#################### CONSTRUCTORS ####################
public:
  /**
   * \brief Constructs an escape game.
   *
   * \param worldFilename The name of the file containing the game world.
   */
  explicit Escape(const std::string& worldFilename);

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /** Override */
  virtual std::string outcome_message() const;

  /** Override */
  virtual void process_input(const tvginput::InputState& inputState);

  /** Override */
  virtual std::string type() const;

  //#################### PRIVATE MEMBER FUNCTIONS ####################
private:
  /** Override */
  virtual void run_step_post();
};

}

#endif
