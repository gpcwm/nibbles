/**
 * nibbles: Game.h
 */

#ifndef H_NIBBLES_GAME
#define H_NIBBLES_GAME

#include <boost/assign/list_of.hpp>

#include <tvginput/InputState.h>

#include "../model/World.h"

namespace nibbles {

/**
 * \brief An instance of a class deriving from this one represents a Nibbles game.
 */
class Game
{
  //#################### PROTECTED VARIABLES ####################
protected:
  /** The current timestamp (the number of steps that have been taken in the game). */
  int m_timestamp;

  /** The game world. */
  World_Ptr m_world;

  //#################### CONSTRUCTORS ####################
protected:
  /**
   * \brief Constructs a game.
   *
   * \param worldFilename The name of the file containing the game world.
   */
  explicit Game(const std::string& worldFilename);

  //#################### DESTRUCTOR ####################
public:
  /**
   * \brief Destroys the game.
   */
  virtual ~Game();

  //#################### PUBLIC ABSTRACT MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Gets the type of the game.
   *
   * \return  The type of the game.
   */
  virtual std::string type() const = 0;

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Gets a message indicating the outcome of the game (if the game has finished).
   *
   * The message will be empty while the game is still in progress.
   *
   * \return  An outcome message for the game, if it has finished, or the empty string if it is still in progress.
   */
  virtual std::string outcome_message() const;

  /**
   * \brief Takes action as relevant based on the current input state.
   *
   * \param inputState  The current input state.
   */
  virtual void process_input(const tvginput::InputState& inputState);

  /**
   * \brief Runs the game for a single step.
   */
  void run_step();

  /**
   * \brief Gets the game world.
   *
   * \return  The game world.
   */
  World_CPtr world() const;

  //#################### PROTECTED MEMBER FUNCTIONS ####################
protected:
  /**
   * \brief Randomly adds a static object of the specified type to an empty cell in the world.
   */
  void randomly_add_static_object(const std::string& type);

  //#################### PRIVATE MEMBER FUNCTIONS ####################
private:
  /**
   * \brief Runs any subclass functionality that is needed after a game step.
   */
  virtual void run_step_post();

  /**
   * \brief Runs any subclass functionality that is needed prior to a game step.
   */
  virtual void run_step_pre();

  //#################### PROTECTED STATIC MEMBER FUNCTIONS ####################
protected:
  /**
   * \brief TODO
   */
  std::vector<std::pair<int,int> > mask_to_cells(const boost::assign_detail::generic_list<int>& mask) const;
};

//#################### TYPEDEFS ####################

typedef boost::shared_ptr<Game> Game_Ptr;
typedef boost::shared_ptr<const Game> Game_CPtr;

}

#endif
