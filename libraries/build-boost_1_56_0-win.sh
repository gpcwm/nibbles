#! /bin/bash -e

LOG=../build-boost_1_56_0.log

# Check that valid parameters have been specified.
if [ $# -ne 1 ] || ([ "$1" != "msvc-11.0" ] && [ "$1" != "msvc-12.0" ])
then
  echo "Usage: build-boost_1_56_0-win.sh {msvc-11.0|msvc-12.0}"
  exit 1
fi

echo "[nibbles] Building Boost 1.56.0 for $1"

if [ -d boost_1_56_0 ]
then
  echo "[nibbles] ...Skipping build (already built)"
  exit
fi

if [ -d boost-setup ]
then
  echo "[nibbles] ...Skipping archive extraction (already extracted)"
else
  echo "[nibbles] ...Extracting archive..."
  /bin/rm -fR tmp
  mkdir tmp
  cd tmp
  tar xzf ../setup/boost_1_56_0/boost_1_56_0.tar.gz
  cd ..
  mv tmp/boost_1_56_0 boost-setup
  rmdir tmp
fi

cd boost-setup

if [ -e b2 ]
then
  echo "[nibbles] ...Skipping bootstrapping (b2 already exists)"
else
  echo "[nibbles] ...Bootstrapping..."
  cmd //c "bootstrap.bat vc12 > $LOG"
fi

echo "[nibbles] ...Running build..."
cmd //c "b2 -j2 --libdir=..\boost_1_56_0\lib --includedir=..\boost_1_56_0\include --abbreviate-paths --with-chrono --with-date_time --with-filesystem --with-regex --with-serialization --with-test --with-thread --build-type=complete --layout=tagged toolset=$1 architecture=x86 address-model=64 install >> $LOG"

echo "[nibbles] ...Fixing headers..."
perl -ibak -pe 's/SPT<void>/SPT<const void>/g' ../boost_1_56_0/include/boost/serialization/shared_ptr_helper.hpp
rm ../boost_1_56_0/include/boost/serialization/shared_ptr_helper.hppbak

echo "[nibbles] ...Finished building Boost 1.56.0."
