#! /bin/bash -e

echo "[nibbles] Extracting SDL2 ttf-2.0.14"

if [ -d SDL2_ttf-2.0.14 ]
then
  echo "[nibbles] ...Skipping archive extraction (already extracted)"
  exit
else
  echo "[nibbles] ...Extracting archive..."
  /bin/rm -fR tmp
  mkdir tmp
  cd tmp
  unzip ../setup/SDL2_ttf-2.0.14/SDL2_ttf-devel-2.0.14-VC.zip > /dev/null 2>&1
  cd ..
  mv tmp/SDL2_ttf-2.0.14 .
  rmdir tmp
fi
