/**
 * nibblescli: TextualRenderer.h
 */

#ifndef H_NIBBLESCLI_TEXTUALRENDERER
#define H_NIBBLESCLI_TEXTUALRENDERER

#include <nibbles/model/World.h>

/**
 * \brief This struct provides functions that can be used to render a Nibbles world as ASCII art.
 */
struct TextualRenderer
{
  //#################### PUBLIC STATIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Renders a Nibbles world as ASCII art.
   *
   * \param world The world to render.
   */
  static void render(const nibbles::World& world);
};

#endif
