/**
 * nibblescli: TextualRenderer.cpp
 */

#include "TextualRenderer.h"
using namespace nibbles;

#include <iostream>

void TextualRenderer::render(const World& world)
{
  const PlayingArea& playingArea = world.playing_area();
  for(int i = 0, height = playingArea.height(); i < height; ++i)
  {
    for(int j = 0, width = playingArea.width(); j < width; ++j)
    {
      std::cout << playingArea.cell(i, j) << ' ';
    }
    std::cout << '\n';
  }
  std::cout << '\n';
}
