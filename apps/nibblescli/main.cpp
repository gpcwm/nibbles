/**
 * nibblescli: main.cpp
 */

//###
#if 1

#include <iostream>

#include <nibbles/games/Escape.h>
#include <nibbles/util/ExecutableFinder.h>
using namespace nibbles;

#include "TextualRenderer.h"

boost::filesystem::path resources_dir()
{
  boost::filesystem::path p = find_executable(); // nibbles/build/bin/apps/nibblescli/nibblescli(.exe)
  p = p.parent_path();                           // nibbles/build/bin/apps/nibblescli/
  p = p / "resources/";                          // nibbles/build/bin/apps/nibblescli/resources/
  return p;
}

int main()
try
{
  Escape game((resources_dir() / "EscapeWorld.txt").string());
  TextualRenderer::render(*game.world());

  std::string line;
  while(std::getline(std::cin, line) && line != "quit")
  {
    game.run_step();
    TextualRenderer::render(*game.world());
  }
  return 0;
}
catch(std::exception& e)
{
  std::cerr << "Error: " << e.what() << '\n';
  return EXIT_FAILURE;
}

#endif
