/**
 * nibblesgui: WindowedRenderer.h
 */

#include "WindowedRenderer.h"

#include <boost/core/null_deleter.hpp>

#if WITH_SDLttf
#include <SDL_ttf.h>
#endif

#include <nibbles/util/ExecutableFinder.h>
using namespace nibbles;

//#################### CONSTRUCTORS ####################

WindowedRenderer::WindowedRenderer(const std::string& title, const nibbles::Game_CPtr& game, int cellSize)
: m_cellSize(cellSize),
  m_game(game),
  m_height(game->world()->playing_area().height() * cellSize + 20),
  m_width(game->world()->playing_area().width() * cellSize + 20)
{
  // Create the window into which to render.
  m_window.reset(SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_width, m_height, 0), &SDL_DestroyWindow);

  // Get the SDL surface associated with the window.
  m_windowSurface = SDL_GetWindowSurface(m_window.get());

  // Load the cell images.
  m_imgApple = load_bitmap(resources_dir() / "apple.bmp");
  m_imgPoisonedApple = load_bitmap(resources_dir() / "poisonedapple.bmp");
}

//#################### PUBLIC MEMBER FUNCTIONS ####################

std::pair<float,float> WindowedRenderer::compute_fractional_window_position(int x, int y) const
{
  return std::make_pair(static_cast<float>(x) / (m_width - 1), static_cast<float>(y) / (m_height - 1));
}

void WindowedRenderer::render() const
{
  // Render the game world.
  const PlayingArea& playingArea = m_game->world()->playing_area();
  for(int i = 0, height = playingArea.height(); i < height; ++i)
  {
    for(int j = 0, width = playingArea.width(); j < width; ++j)
    {
      render_cell(i, j, playingArea.cell(i, j));
    }
  }

  // If the game is over, render a message displaying the final result.
  render_text(m_game->outcome_message(), "FreeSans", 48, m_width / 2, m_height / 2, 255, 0, 0, ALIGN_CENTRE);

  // Swap the front and back buffers.
  SDL_UpdateWindowSurface(m_window.get());
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

WindowedRenderer::SDL_Surface_Ptr WindowedRenderer::load_bitmap(const boost::filesystem::path& path) const
{
  SDL_Surface_Ptr img(SDL_LoadBMP(path.string().c_str()), SDL_FreeSurface);
  img.reset(SDL_ConvertSurface(img.get(), m_windowSurface->format, 0), SDL_FreeSurface);
  return img;
}

void WindowedRenderer::render_cell(int i, int j, int value) const
{
  // FIXME: This is a big hack for now.
  SDL_Rect rect = { 10 + j * m_cellSize, 10 + i * m_cellSize, m_cellSize, m_cellSize };

  Object_Ptr object = m_game->world()->object_database().object(value);
  if(object && object->type() == "Apple")
  {
    SDL_BlitScaled(m_imgApple.get(), NULL, m_windowSurface, &rect);
  }
  else if(object && object->type() == "PoisonedApple")
  {
    SDL_BlitScaled(m_imgPoisonedApple.get(), NULL, m_windowSurface, &rect);
  }
  else
  {
    const int colours[] = {
      0, 0, 0,
      255, 255, 255,
      255, 0, 0,
      0, 255, 0,
      0, 0, 255,
    };

    int offset = value * 3;
    int count = sizeof(colours) / sizeof(int);

    int r, g, b;
    if(object && object->type() == "Exit")
    {
      r = 255;
      g = 0;
      b = 255;
    }
    else
    {
      r = colours[offset % count];
      g = colours[(offset + 1) % count];
      b = colours[(offset + 2) % count];
    }

    SDL_FillRect(m_windowSurface, &rect, SDL_MapRGB(m_windowSurface->format, r, g, b));
  }
}

void WindowedRenderer::render_text(const std::string& text, const std::string& fontName, int size, int x, int y, int r, int g, int b, TextAlignment alignment) const
{
#if WITH_SDLttf
  boost::shared_ptr<TTF_Font> font(TTF_OpenFont((resources_dir() / (fontName + ".ttf")).string().c_str(), size), TTF_CloseFont);

  if(alignment == ALIGN_CENTRE)
  {
    int textWidth, textHeight;
    TTF_SizeText(font.get(), text.c_str(), &textWidth, &textHeight);
    x -= textWidth / 2;
    y -= textHeight / 2;
  }

  SDL_Color colour = { r, g, b };
  SDL_Surface *textSurface = TTF_RenderText_Solid(font.get(), text.c_str(), colour);
  SDL_Rect rect = { x, y, 0, 0 };
  SDL_BlitSurface(textSurface, NULL, m_windowSurface, &rect);
#endif
}

//#################### PRIVATE STATIC MEMBER FUNCTIONS ####################

boost::filesystem::path WindowedRenderer::resources_dir()
{
  boost::filesystem::path p = find_executable(); // nibbles/build/bin/apps/nibblesgui/nibblesgui(.exe)
  p = p.parent_path();                           // nibbles/build/bin/apps/nibblesgui/
  p = p / "resources/";                          // nibbles/build/bin/apps/nibblesgui/resources/
  return p;
}
