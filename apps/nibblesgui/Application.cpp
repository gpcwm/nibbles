/**
 * nibblesgui: Application.cpp
 */

#include "Application.h"
using namespace nibbles;
using namespace tvginput;

//#################### CONSTRUCTORS ####################

Application::Application(const Game_Ptr& game)
: m_game(game)
{
  m_renderer.reset(new WindowedRenderer("Nibbles - " + game->type(), game));
}

//#################### PUBLIC MEMBER FUNCTIONS ####################

void Application::run()
{
  int lastStepTime = 0;

  for(;;)
  {
    if(!process_events() || m_inputState.key_down(KEYCODE_ESCAPE)) return;

    // If the game is still running:
    if(m_game->outcome_message() == "")
    {
      // Take action as relevant based on the current input state.
      m_game->process_input(m_inputState);

      // If it has been long enough since the last game step, run another step.
      int currentTime = SDL_GetTicks();
      if(currentTime - lastStepTime >= 500)
      {
        lastStepTime = currentTime;
        m_game->run_step();
      }
    }

    // Render the world.
    m_renderer->render();
  }
}

//#################### PRIVATE MEMBER FUNCTIONS ####################

void Application::handle_key_down(const SDL_Keysym& keysym)
{
  m_inputState.press_key(static_cast<Keycode>(keysym.sym));

  // TEMPORARY: Allow single-stepping of the game.
  if(keysym.sym == KEYCODE_RETURN) m_game->run_step();
}

void Application::handle_key_up(const SDL_Keysym& keysym)
{
  m_inputState.release_key(static_cast<Keycode>(keysym.sym));
}

void Application::handle_mousebutton_down(const SDL_MouseButtonEvent& e)
{
  std::pair<float,float> p = m_renderer->compute_fractional_window_position(e.x, e.y);

  switch(e.button)
  {
    case SDL_BUTTON_LEFT:
      m_inputState.press_mouse_button(MOUSE_BUTTON_LEFT, p.first, p.second);
      break;
    case SDL_BUTTON_MIDDLE:
      m_inputState.press_mouse_button(MOUSE_BUTTON_MIDDLE, p.first, p.second);
      break;
    case SDL_BUTTON_RIGHT:
      m_inputState.press_mouse_button(MOUSE_BUTTON_RIGHT, p.first, p.second);
      break;
    default:
      break;
  }
}

void Application::handle_mousebutton_up(const SDL_MouseButtonEvent& e)
{
  switch(e.button)
  {
    case SDL_BUTTON_LEFT:
      m_inputState.release_mouse_button(MOUSE_BUTTON_LEFT);
      break;
    case SDL_BUTTON_MIDDLE:
      m_inputState.release_mouse_button(MOUSE_BUTTON_MIDDLE);
      break;
    case SDL_BUTTON_RIGHT:
      m_inputState.release_mouse_button(MOUSE_BUTTON_RIGHT);
      break;
    default:
      break;
  }
}

bool Application::process_events()
{
  SDL_Event event;
  while(SDL_PollEvent(&event))
  {
    switch(event.type)
    {
      case SDL_KEYDOWN:
        handle_key_down(event.key.keysym);
        break;
      case SDL_KEYUP:
        handle_key_up(event.key.keysym);
        break;
      case SDL_MOUSEBUTTONDOWN:
        handle_mousebutton_down(event.button);
        break;
      case SDL_MOUSEBUTTONUP:
        handle_mousebutton_up(event.button);
        break;
      case SDL_MOUSEMOTION:
      {
        std::pair<float,float> p = m_renderer->compute_fractional_window_position(event.motion.x, event.motion.y);
        m_inputState.set_mouse_position(p.first, p.second);
        break;
      }
      case SDL_QUIT:
        return false;
      default:
        break;
    }
  }

  return true;
}
