/**
 * nibblesgui: WindowedRenderer.h
 */

#ifndef H_NIBBLESGUI_WINDOWEDRENDERER
#define H_NIBBLESGUI_WINDOWEDRENDERER

#include <boost/filesystem.hpp>

// Prevent SDL from trying to define M_PI.
#define HAVE_M_PI

#include <SDL.h>

#include <nibbles/games/Game.h>

/**
 * \brief An instance of this class can be used to render a Nibbles game to a window.
 */
class WindowedRenderer
{
  //#################### TYPEDEFS ####################
private:
  typedef boost::shared_ptr<SDL_Surface> SDL_Surface_Ptr;
  typedef boost::shared_ptr<SDL_Window> SDL_Window_Ptr;

  //#################### ENUMERATIONS ####################
private:
  /**
   * \brief The values of this enumeration denote the different ways in which text can be aligned.
   */
  enum TextAlignment
  {
    ALIGN_CENTRE,
    ALIGN_LEFT_TOP,
  };

  //#################### PRIVATE VARIABLES ####################
private:
  /** The size of cell to use when rendering the Nibbles game world. */
  int m_cellSize;

  /** The Nibbles game to render. */
  nibbles::Game_CPtr m_game;

  /** The height of the window. */
  int m_height;

  /** An image of an apple. */
  SDL_Surface_Ptr m_imgApple;

  /** An image of a poisoned apple. */
  SDL_Surface_Ptr m_imgPoisonedApple;

  /** The width of the window. */
  int m_width;

  /** The window into which to render. */
  SDL_Window_Ptr m_window;

  /** The SDL surface associated with the window. */
  SDL_Surface *m_windowSurface;

  //#################### CONSTRUCTORS ####################
public:
  /**
   * \brief Constructs a windowed renderer.
   *
   * \param title     The title of the window.
   * \param world     The Nibbles game to render.
   * \param cellSize  The size of cell to use when rendering the Nibbles game world.
   */
  WindowedRenderer(const std::string& title, const nibbles::Game_CPtr& game, int cellSize = 20);

  //#################### COPY CONSTRUCTOR & ASSIGNMENT OPERATOR ####################
private:
  // Deliberately private and unimplemented.
  WindowedRenderer(const WindowedRenderer&);
  WindowedRenderer& operator=(const WindowedRenderer&);

  //#################### PUBLIC MEMBER FUNCTIONS ####################
public:
  /**
   * \brief Computes the fractional position of point (x,y) in the window.
   *
   * \param x The x coordinate of the point whose fractional position is to be computed.
   * \param y The y coordinate of the point whose fractional position is to be computed.
   * \return  The fractional position of the specified point in the window.
   */
  std::pair<float,float> compute_fractional_window_position(int x, int y) const;

  /**
   * \brief Renders the Nibbles world.
   */
  void render() const;

  //#################### PRIVATE MEMBER FUNCTIONS ####################
private:
  /**
   * \brief Loads a bitmap into an SDL surface.
   *
   * \param filename  The path to the bitmap.
   * \return          The SDL surface containing the loaded bitmap.
   */
  SDL_Surface_Ptr load_bitmap(const boost::filesystem::path& path) const;

  /**
   * \brief Renders a cell in the Nibbles world.
   *
   * \param i     The row index of the cell.
   * \param j     The column index of the cell.
   * \param value The value of the cell.
   */
  void render_cell(int i, int j, int value) const;

  /**
   * \brief TODO
   */
  void render_text(const std::string& text, const std::string& fontName, int size, int x, int y, int r, int g, int b, TextAlignment alignment) const;

  //#################### PRIVATE STATIC MEMBER FUNCTIONS ####################
private:
  /**
   * \brief Gets the path to the resources directory.
   *
   * \return  The path to the resources directory.
   */
  static boost::filesystem::path resources_dir();
};

#endif
