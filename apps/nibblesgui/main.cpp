/**
 * nibblesgui: main.cpp
 */

//###
#if 1

#include <cstdlib>
#include <ctime>
#include <iostream>

#include <boost/shared_ptr.hpp>

#include <SDL.h>

#if WITH_SDLttf
#include <SDL_ttf.h>
#endif

#include <nibbles/games/Escape.h>
#include <nibbles/util/ExecutableFinder.h>
using namespace nibbles;

#include "Application.h"

void quit(const std::string& message, int code = EXIT_FAILURE)
{
  std::cerr << message << '\n';
  SDL_Quit();
  exit(code);
}

boost::filesystem::path resources_dir()
{
  boost::filesystem::path p = find_executable(); // nibbles/build/bin/apps/nibblesgui/nibblesgui(.exe)
  p = p.parent_path();                           // nibbles/build/bin/apps/nibblesgui/
  p = p / "resources/";                          // nibbles/build/bin/apps/nibblesgui/resources/
  return p;
}

int main(int argc, char *argv[])
try
{
  // Seed the random number generator.
  srand(static_cast<unsigned int>(time(NULL)));

  // Initialise SDL.
  if(SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    quit("Could not initialise SDL");
  }

#if WITH_SDLttf
  // Initialise SDL_ttf.
  if(TTF_Init() != 0)
  {
    quit("Could not initialise SDL_ttf");
  }
#endif

  // Run the application.
  Application app(Game_Ptr(new Escape((resources_dir() / "EscapeWorld.txt").string())));
  app.run();

#if WITH_SDLttf
  // Shutdown SDL_ttf.
  TTF_Quit();
#endif

  // Shutdown SDL.
  SDL_Quit();

  return 0;
}
catch(std::exception& e)
{
  std::cerr << "Error: " << e.what() << '\n';
  return EXIT_FAILURE;
}

#endif
