#! /bin/bash -e

# Check that valid parameters have been specified.
if [ $# -ne 2 ] || ([ "$1" != "11" ] && [ "$1" != "12" ]) || ([ "$2" != "Debug" ] && [ "$2" != "Release" ])
then
  echo "Usage: build-win.sh {11|12} {Debug|Release}"
  exit
fi

# Check that msbuild is on the system path.
./require-msbuild.sh

cd libraries
./build-boost_1_56_0-win.sh "msvc-$1.0"
./build-SDL2-2.0.3-win.sh "Visual Studio $1 Win64"
./extract-SDL2_ttf-2.0.14-win.sh
cd ..

echo "[nibbles] Building nibbles"

if [ ! -d build ]
then
  mkdir build
  cd build

  # Note: We need to configure twice to handle conditional building.
  echo "[nibbles] ...Configuring using CMake..."
  cmake -G "Visual Studio $1 Win64" ..
  cmake ..

  cd ..
fi

cd build

echo "[nibbles] ...Running build..."
cmd //c "msbuild /p:Configuration=$2 nibbles.sln"

echo "[nibbles] ...Installing..."
cmd //c "msbuild /p:Configuration=$2 INSTALL.vcxproj"

echo "[nibbles] ...Finished building nibbles."
