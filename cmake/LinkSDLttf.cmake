####################
# LinkSDLttf.cmake #
####################

IF(WITH_SDLttf)
  TARGET_LINK_LIBRARIES(${targetname} ${SDLttf_LIBRARY})

  IF(MSVC_IDE)
    FILE(GLOB RUNTIMELIBS "${SDLttf_LIBRARY_DIR}/*.dll")
  ENDIF()

  FOREACH(RUNTIMELIB ${RUNTIMELIBS})
    ADD_CUSTOM_COMMAND(TARGET ${targetname} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different ${RUNTIMELIB} "$<TARGET_FILE_DIR:${targetname}>")
  ENDFOREACH()
ENDIF()
