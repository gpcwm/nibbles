###################
# UseSDLttf.cmake #
###################

OPTION(WITH_SDLttf "Build with SDL_ttf support?" OFF)

IF(WITH_SDLttf)
  FIND_PATH(SDLttf_INCLUDE_DIR SDL_ttf.h HINTS "${PROJECT_SOURCE_DIR}/libraries/SDL2_ttf-2.0.14/include")
  FIND_PATH(SDLttf_LIBRARY_DIR LICENSE.freetype.txt HINTS "${PROJECT_SOURCE_DIR}/libraries/SDL2_ttf-2.0.14/lib/x64")
  FIND_LIBRARY(SDLttf_LIBRARY SDL2_ttf HINTS ${SDLttf_LIBRARY_DIR})
  INCLUDE_DIRECTORIES(${SDLttf_INCLUDE_DIR})
  ADD_DEFINITIONS(-DWITH_SDLttf)
ENDIF()
